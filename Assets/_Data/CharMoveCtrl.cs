using UnityEngine;

public abstract class CharMoveCtrl : SaiBehaviour
{
    public CharacterController characterController;

    [Header("Move")]
    public bool grounded = false;

    public bool isMoving = false;
    public bool isTurnRight = true;

    [Header("Walking")]
    public Vector3 movement = new Vector3(0, 0, 0);
    public float speed = 3f;
    public float moveHorizontal = 0f;
    public float lastDirection = 0f;

    [Header("Jumping")]
    public bool isJump = false;

    public float jumpHeight = 5f;
    public float fallingSpeed = 7f;

    [Header("Mouse")]
    public bool lookAtMouse = true;

    public Vector3 mouseInWorld;
    public Vector3 mouseToChar;

    protected abstract Animator Animator();

    protected abstract Transform Model();

    protected abstract CharacterController CharacterController();

    protected override void Update()
    {
        base.Update();
        this.UpdateMoving();
    }

    protected virtual void UpdateMoving()
    {
        this.grounded = this.IsGround();
        this.ResetYAxis();
        this.Jumping();
        this.Falling();
        this.Moving();
        this.Turning();
        this.Animation();
        this.CharacterController().Move(this.movement * Time.deltaTime);
    }

    public bool IsGround()
    {
        return this.CharacterController().isGrounded;
    }

    public virtual void Jumping()
    {
        if (!this.grounded) return;
        if (this.isJump) this.movement.y = this.jumpHeight;
    }

    protected virtual void Moving()
    {
        if (!this.grounded) return;
        this.movement.x = this.speed * this.moveHorizontal;
    }

    protected virtual void Falling()
    {
        if (this.grounded) return;
        this.movement.y -= this.fallingSpeed * Time.deltaTime;
    }

    protected virtual void Turning()
    {
        if (this.lookAtMouse) this.LookAtMouse();
        else this.TurnByMovement();
        Vector3 tempScale = this.Model().localScale;
        if (this.isTurnRight) tempScale.x = 1;
        else tempScale.x = -1;
        this.Model().localScale = tempScale;
    }

    protected virtual void TurnByMovement()
    {
        this.isTurnRight = true;
        if (this.moveHorizontal != 0) this.lastDirection = this.moveHorizontal;
        if (this.lastDirection < 0) this.isTurnRight = false;
    }

    protected virtual void LookAtMouse()
    {
        this.mouseToChar = this.mouseInWorld - this.Model().position;
        this.isTurnRight = true;
        if (this.mouseToChar.x != 0) this.lastDirection = this.mouseToChar.x;
        if (this.mouseToChar.x < 0) this.isTurnRight = false;
    }

    protected bool IsMoving()
    {
        this.isMoving = false;
        if (this.isJump) this.isMoving = true;
        if (this.moveHorizontal != 0) this.isMoving = true;
        return this.isMoving;
    }

    protected virtual void Animation()
    {
        if (this.IsMoving()) this.AnimWalking();
        else this.AnimIdle();
    }

    protected virtual void AnimIdle()
    {
        this.Animator().SetInteger("Stage", 0);
    }

    protected virtual void AnimWalking()
    {
        this.Animator().SetInteger("Stage", 1);
    }

    protected virtual void ResetYAxis()
    {
        if (this.grounded && this.movement.y < 0)
        {
            this.movement.y = 0f;
        }
    }
}
