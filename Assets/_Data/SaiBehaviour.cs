using UnityEngine;

public class SaiBehaviour : MonoBehaviour
{
    protected virtual void Awake()
    {
        this.LoadComponents();
        this.LoadValue();
    }

    protected virtual void Reset()
    {
        this.LoadComponents();
        this.LoadValue();
    }
    
    protected virtual void OnEnable()
    {
        // For override
    }

    protected virtual void LoadComponents()
    {
        // For override
    }
    
    protected virtual void LoadValue()
    {
        // For override
    }

    protected virtual void Update()
    {
        // For override
    }

    protected virtual void FixedUpdate()
    {
        // For override
    }
    
    protected virtual void Start()
    {
        // For override
    }
    
    protected virtual void LateUpdate()
    {
        // For override
    }
}
