using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCtrl : SaiBehaviour
{
    protected float smoothing = 10;
    protected Vector3 playerPos;
    protected float offSetY = 1f;

    protected override void Start()
    {
        base.Awake();
        playerPos = PlayerController.instance.transform.position;
        transform.position = new Vector3(this.playerPos.x, this.playerPos.y, transform.position.z);
    }

    protected override void LateUpdate()
    {
        base.Update();
        this.FollowPlayer();
    }

    protected virtual void FollowPlayer()
    {
        // TODO: If player jumping dont move the camera in y axis
        playerPos = PlayerController.instance.playerMovement.transform.position;
        Vector3 targetPos = new Vector3(this.playerPos.x, this.playerPos.y + this.offSetY, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, targetPos, this.smoothing * Time.deltaTime);
    }
}
