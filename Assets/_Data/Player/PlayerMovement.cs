using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : CharMoveCtrl
{
    public PlayerController playerController;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadCharController();
    }

    protected virtual void LoadCharController()
    {
        if (this.characterController != null) return;
        this.playerController = transform.parent.GetComponent<PlayerController>();
        this.characterController = this.playerController.charController;
        Debug.Log(transform.name + ": LoadCharController", gameObject);
    }

    protected override void Moving()
    {
        // Player can't move when attacking
        if (PlayerController.instance.playerCombat.IsAttacking())
        {
            this.movement.x = 0f;
            return;
        }
        this.RunFX();
        base.Moving();
    }

    protected override Animator Animator()
    {
        return this.playerController.animator;
    }

    protected override Transform Model()
    {
        return this.playerController.playerModel;
    }

    protected override CharacterController CharacterController()
    {
        return this.playerController.charController;
    }

    protected virtual void RunFX()
    {
        Transform fx = FXManager.instance.GetFX("Dashwind");
        fx.parent = this.playerController.playerModel;
        fx.position = this.playerController.playerModel.position;
        if (this.isTurnRight) fx.position += new Vector3(-0.8f, -1f, 0);
        else fx.position += new Vector3(0.8f, -1f, 0);
        // Dashwind dashwind = fx.GetComponent<Dashwind>();
        // dashwind.Turning(this.isTurnRight);
        if (this.moveHorizontal != 0) fx.gameObject.SetActive(true);
        else fx.gameObject.SetActive(false);
    }
}