using System;
using UnityEngine;

public class PlayerDamageReceiver : DamageReceiver
{
    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.hp = 100;
        this.hpMax = 100;
    }
}