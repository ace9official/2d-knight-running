using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : SaiBehaviour
{
    public static PlayerController instance;

    public Transform playerModel;
    public PlayerMovement playerMovement;
    public PlayerCombat playerCombat;
    public Animator animator;
    public CharacterController charController;
    public PlayerDamageReceiver damageReceiver;

    protected override void Awake()
    {
        base.Awake();
        if (PlayerController.instance != null) Debug.LogError("Only 1 PlayerController allow");
        PlayerController.instance = this;
    }

    protected override void LoadComponents()
    {
        this.LoadChar();
        this.LoadCharController();
    }

    protected virtual void LoadChar()
    {
        if (this.playerModel != null) return;
        this.playerModel = transform.Find("Model");
        this.animator = this.playerModel.GetComponent<Animator>();
        this.playerMovement = transform.Find("PlayerMovement").GetComponent<PlayerMovement>();
        this.damageReceiver = transform.Find("DamageReceiver").GetComponent<PlayerDamageReceiver>();
        this.playerCombat = transform.Find("PlayerCombat").GetComponent<PlayerCombat>();
        Debug.Log(transform.name + ": LoadChar", gameObject);
    }

    protected virtual void LoadCharController()
    {
        if (this.charController != null) return;
        this.charController = GetComponent<CharacterController>();
        this.charController.center = new Vector3(0, -0.6f, 0);
        this.charController.radius = 0.4f;
        this.charController.height = 1.4f;
        Debug.Log(transform.name + ": LoadCharController", gameObject);
    }
}
