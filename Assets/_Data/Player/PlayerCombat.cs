using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : Combat
{
    public PlayerController playerController;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadPlayerCtrl();
    }

    protected virtual void LoadPlayerCtrl()
    {
        if (this.playerController != null) return;
        this.playerController = transform.GetComponentInParent<PlayerController>();
        Debug.Log(gameObject.name + ": LoadPlayerCtrl", transform);
    }

    public override void SpawnSkill()
    {
        if (this.skillReleased) return;
        this.skillReleased = true;
        //Debug.Log("SpawnSkill");
        Transform fx = FXManager.instance.Spawn("Fireball");
        fx.position = this.GetStrikePoint();
       // fx.parent = FXManager.instance.fxHolder;
        FireballFly fireballFly = fx.GetComponent<FireballFly>();
        fireballFly.Turing(PlayerController.instance.playerMovement.isTurnRight);
        fx.gameObject.SetActive(true);
    }

    protected override Animator Animator()
    {
        return this.playerController.animator;
    }

    public override CharMoveCtrl CharMoveCtrl()
    {
        return this.playerController.playerMovement;
    }
}