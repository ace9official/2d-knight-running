using UnityEngine;

public class CharAttackAnim : MonoBehaviour
{
    protected virtual void SpawnSkill()
    {
        PlayerController.instance.playerCombat.SpawnSkill();
        //Debug.Log("SpawnSkill");
    }

    protected virtual void AttackFinish()
    {
        PlayerController.instance.playerCombat.AttackFinish();
//        Debug.Log("AttackFinish");
    }
}