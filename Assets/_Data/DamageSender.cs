using UnityEngine;

public class DamageSender : SaiBehaviour
{
    [Header("DamageSender")]
    [SerializeField] protected int damage = 1;

    public virtual int Damage()
    {
        return damage;
    }
}