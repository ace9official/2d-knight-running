using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// ReSharper disable All

public class Spawner : SaiBehaviour
{
    public static Spawner instance;

    [Header("Obj Rfr")]
    [SerializeField] protected Transform listSpawnPosObj;
    [SerializeField] protected List<Transform> listSpawnPos;
    [SerializeField] protected string listSpawnPosName = "SpawnPos";

    [Header("")]
    [SerializeField] protected Transform spawnHolder;
    [SerializeField] protected string spawnHolderName = "SpawnHolder";

    [Header("")]
    [SerializeField] protected Transform despawnHolder;
    [SerializeField] protected string despawnHolderName = "DespawnHolder";

    [Header("Spawner")]
    [SerializeField] protected List<Transform> prefabs;
    [SerializeField] protected int spawnCount = 0;
    [SerializeField] protected int spawnLimit = 10;
    [SerializeField] protected float spawnTimer = 0f;
    [SerializeField] protected float spawnDelay = 4f;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadSpawnPos();
        this.LoadPrefab();
    }

    protected override void Awake()
    {
        base.Awake();
        if (Spawner.instance != null) Debug.LogError("Only 1 Spawner allow");
        Spawner.instance = this;
        this.HideAll();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        this.Spawning();
        this.spawnCount = this.spawnHolder.childCount;
    }

    protected virtual void LoadSpawnPos()
    {
        if (this.listSpawnPos.Count > 0) return;
        this.listSpawnPosObj = GameObject.Find(this.listSpawnPosName).transform;
        this.spawnHolder = GameObject.Find(this.spawnHolderName).transform;
        this.despawnHolder = GameObject.Find(this.despawnHolderName).transform;
        foreach (Transform child in this.listSpawnPosObj)
        {
            this.listSpawnPos.Add(child);
        }
        Debug.Log(transform.name + ": LoadSpawnPos", gameObject);
    }

    protected virtual void LoadPrefab()
    {
        if (this.prefabs.Count > 0) return;
        foreach (Transform child in transform)
        {
            this.prefabs.Add(child);
        }
        Debug.Log(transform.name + ": LoadPrefab", gameObject);
    }

    protected virtual void HideAll()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    protected virtual void Spawning()
    {
        this.spawnTimer += Time.fixedDeltaTime;
        if (this.spawnTimer <= this.spawnDelay) return;
        if (!this.CanSpawn()) return;
        this.spawnTimer = 0;
//        Debug.Log("Spawned");
        Transform prefab = this.GetObj2Spawn();
        Vector3 pos = this.GetPos2Spawn();
        Transform newObj = SpawnObj(prefab, pos, this.spawnHolder);
    }

    public virtual bool CanSpawn()
    {
        if (MySceneManger.instance.IsSceneOVer()) return false;
        return this.SpawnCount() < this.spawnLimit;
    }

    public virtual int SpawnCount()
    {
        return this.spawnHolder.childCount;
    }

    protected virtual Transform GetObj2Spawn()
    {
        int rand = Random.Range(0, this.prefabs.Count);
        return this.prefabs[rand];
    }

    protected virtual Vector3 GetPos2Spawn()
    {
        int rand = Random.Range(0, this.listSpawnPos.Count);
        return this.listSpawnPos[rand].position;
    }

    public virtual Transform SpawnObj(Transform prefab, Vector3 pos)
    {
        Transform spawnedObj = this.GetObjFromDespawnHolder(prefab);
        if (spawnedObj == null) spawnedObj = Instantiate(prefab);
        spawnedObj.name = prefab.name;
        spawnedObj.position = pos;
        spawnedObj.gameObject.SetActive(true);
        return spawnedObj;
    }

    protected virtual Transform GetObjFromDespawnHolder(Transform prefab)
    {
        int childCount = this.despawnHolder.childCount;
        //Debug.Log("childCount: " + childCount);
        if (childCount < 1) return null;
        foreach (Transform child in this.despawnHolder)
        {
            if (child.name == prefab.name) return child;
            //Debug.Log(child);
        }
        return null;
    }

    public virtual Transform SpawnObj(Transform prefab, Vector3 pos, Transform parent)
    {
        Transform newObj = this.SpawnObj(prefab, pos);
        newObj.parent = parent;
        return newObj;
    }

    public virtual void Despawn(Transform obj)
    {
        obj.gameObject.SetActive(false);
        obj.parent = this.despawnHolder;
    }
}
