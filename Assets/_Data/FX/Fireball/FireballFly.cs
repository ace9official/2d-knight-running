using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class FireballFly : SaiBehaviour
{
    public bool isTurnRight = true;
    [SerializeField] protected float speed = 9f;
    [SerializeField] protected Transform model;
    [SerializeField] protected DamageSender damageSender;
    [SerializeField] protected Collider _collider;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadModel();
        this.LoadDamageSender();
    }

    protected virtual void LoadModel()
    {
        if (this.model != null) return;
        this.model = transform.Find("Model");
        Debug.Log(gameObject + ": LoadModel", gameObject);
    }

    protected virtual void LoadDamageSender()
    {
        if (this.damageSender != null) return;
        this.damageSender = transform.Find("DamageSender").GetComponent<DamageSender>();
        this._collider = GetComponent<Collider>();
        this._collider.isTrigger = true;
        Debug.Log(gameObject + ": LoadDamageSender", gameObject);
    }

    protected override void Update()
    {
        base.Update();
        this.Flying();
    }

    protected virtual void Flying()
    {
        Vector3 direction = transform.right;
        if (!this.isTurnRight) direction *= -1;
        transform.position += direction * (this.speed * Time.deltaTime);
    }

    public virtual void Turing(bool isTurnRight)
    {
        this.isTurnRight = isTurnRight;
        Vector3 scale = this.model.localScale;
        if (this.isTurnRight) scale.x = 1f;
        else scale.x = -1f;
        this.model.localScale = scale;
    }

    private void OnTriggerEnter(Collider other)
    {
        DamageReceiver damageReceiver = other.GetComponentInChildren<DamageReceiver>();
        if (!damageReceiver) return;
        damageReceiver.Deduct(this.damageSender.Damage());
        //Debug.Log(other.gameObject.name);
      FXManager.instance.Despawn(transform);
        Transform fx = FXManager.instance.Spawn("Explosion");
        fx.parent = FXManager.instance.fxHolder;
        fx.position = transform.position;
        fx.gameObject.SetActive(true);
    }
}