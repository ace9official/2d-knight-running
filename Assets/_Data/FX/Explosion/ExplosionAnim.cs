using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionAnim : MonoBehaviour
{
    protected virtual void Destroy()
    {
        FXManager.instance.Despawn(transform);
    }
}