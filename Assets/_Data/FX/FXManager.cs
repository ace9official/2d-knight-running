using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXManager : SaiBehaviour
{
    public static FXManager instance;

    [SerializeField] public List<Transform> fxs = new List<Transform>();
    [SerializeField] public Transform fxHolder;

    protected override void Awake()
    {
        base.Awake();
        if (FXManager.instance != null) Debug.LogError("Only 1 FXManager allow");
        FXManager.instance = this;
        this.HideAll();
        InvokeRepeating("ClearFXHolder", 5, 5);
    }

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadFXS();
        this.LoadFXHolder();
    }

    protected virtual void LoadFXS()
    {
        if (this.fxs.Count > 0) return;
        foreach (Transform child in transform)
        {
            this.fxs.Add(child);
        }
        Debug.Log(transform.name + ": LoadFXS", gameObject);
    }

    protected virtual void LoadFXHolder()
    {
        if (this.fxHolder != null) return;
        this.fxHolder = GameObject.Find("FXHolder").transform;
    }

    protected virtual void HideAll()
    {
        foreach (Transform fx in fxs)
        {
            fx.gameObject.SetActive(false);
        }
    }

    public virtual Transform Spawn(string fxName)
    {
        foreach (Transform child in this.fxs)
        {
            if (child.name == fxName)
            {
                Transform fx = Instantiate(child);
                return fx;
            }
        }
        return null;
    }

    public virtual void Despawn(Transform tranObj)
    {
        Destroy(tranObj.gameObject);
    }

    // Called in InvokeRepeating
    public virtual void ClearFXHolder()
    {
        if (this.fxHolder.childCount < 1) return;
        foreach (Transform child in this.fxHolder)
        {
            Despawn(child);
        }
    }

    public virtual Transform GetFX(string fxName)
    {
        foreach (Transform child in this.fxs)
        {
            if (child.name == fxName)
            {
                Transform fx = child;
                return fx;
            }
        }
        return null;
    }
}