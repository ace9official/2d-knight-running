using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIBossWarning : SaiBehaviour
{
    [SerializeField] protected TextMeshProUGUI countDown;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.countDown = this.GetComponent<TextMeshProUGUI>();
    }
    
    protected override void FixedUpdate()
    {
        float timeRemain = BossSpawner.bossInstance.BossCountDown();
        this.countDown.text = "Boss Is Coming: " + Mathf.RoundToInt(timeRemain);
        if (timeRemain <0) return;
    }
}
