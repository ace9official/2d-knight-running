using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UINextScene : MonoBehaviour
{
    public virtual void Next()
    {
        MySceneManger.instance.NextScene();
    }
}
