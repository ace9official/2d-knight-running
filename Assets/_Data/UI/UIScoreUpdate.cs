using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIScoreUpdate : SaiBehaviour
{
    public TextMeshProUGUI score;

    protected override void FixedUpdate()
    {
        this.score.text = ScoreManager.instance.GetScore().ToString();
    }
}
