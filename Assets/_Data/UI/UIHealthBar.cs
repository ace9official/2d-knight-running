using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class UIHealthBar : SaiBehaviour
{
    [SerializeField] protected Image healthBar;
    [SerializeField] protected float currentHealth;
    [SerializeField] protected float maxHealth;
    [SerializeField] protected TextMeshProUGUI healthText;
    [SerializeField] protected Vector3 offSet;

    protected override void OnEnable()
    {
        this.SetPos();
    }

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadHealthBar();
        this.LoadHealthText();
    }

    protected void LoadHealthBar()
    {
        if (this.healthBar != null) return;
        this.healthBar = GetComponent<Image>();
        Debug.Log(transform.name + ": LoadHealthBar" , gameObject);
    }

    protected void LoadHealthText()
    {
        if (this.healthText != null) return;
        this.healthText = this.GetComponentInChildren<TextMeshProUGUI>();
        Debug.Log(transform.name + ": LoadHealthText" , gameObject);
    }

    protected override void FixedUpdate()
    {
        this.GetHealth();
        this.UpdateHealthBar();
        this.UpdateHealthText();
        this.SetPos();
    }

    protected virtual void UpdateHealthBar()
    {
        this.healthBar.fillAmount = this.currentHealth / this.maxHealth;
    }

    protected virtual void UpdateHealthText()
    {
        this.healthText.text = this.currentHealth.ToString();
    }

    protected virtual void GetHealth()
    {
        this.currentHealth = PlayerController.instance.damageReceiver.hp;
        if (this.currentHealth < 1) this.currentHealth = 0;
        this.maxHealth = PlayerController.instance.damageReceiver.hpMax;
    }

    protected virtual void SetPos()
    {
    }
}
