using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIResetScene : MonoBehaviour
{
    public virtual void Reset()
    {
        MySceneManger.instance.ResetScene();
    }
}
