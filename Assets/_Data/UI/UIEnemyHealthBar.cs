using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class UIEnemyHealthBar : UIHealthBar
{
    [SerializeField] protected MonsterCtrl monsterCtrl;

    protected override void LoadValue()
    {
        //this.offSet = new Vector3(0, -1.25f, 0);
    }

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadMonsterCtrl();
    }

    protected virtual void LoadMonsterCtrl()
    {
        if (this.monsterCtrl != null) return;
        this.monsterCtrl = this.GetComponentInParent<MonsterCtrl>();
    }

    protected override void GetHealth()
    {
        this.currentHealth = this.monsterCtrl.damageReceiver.hp;
        if (this.currentHealth < 1) this.currentHealth = 0;
        this.maxHealth = this.monsterCtrl.damageReceiver.hpMax;
    }

    protected override void SetPos()
    {
        transform.parent.position = this.monsterCtrl.model.position + this.offSet;
    }
}
