using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UISceneOver : SaiBehaviour
{
    [SerializeField] protected GameObject btnNext;
    [SerializeField] protected GameObject btnReset;
    [SerializeField] protected GameObject btnResetGame;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadBtn();
    }

    protected virtual void LoadBtn()
    {
        if (this.btnNext != null) return;
        this.btnNext = GameObject.Find("BtnNext");
        this.btnReset = GameObject.Find("BtnReset");
        this.btnResetGame = GameObject.Find("BtnResetGame");
    }

    protected override void Awake()
    {
        base.Awake();
        InvokeRepeating(nameof(this.CheckSceneOver), 0.01f, 0.5f);
    }

    protected virtual void CheckSceneOver()
    {
        if (MySceneManger.instance.IsSceneOVer()) this.Show();
        else this.Hide();
    }

    protected virtual void Show()
    {
        gameObject.SetActive(true);
        if (MySceneManger.instance.IsMaxLevel())
        {
            this.btnNext.SetActive(false);
            this.btnResetGame.SetActive(true);
        }
        else
        {
            this.btnNext.SetActive(true);
            this.btnResetGame.SetActive(false);
        }
    }

    protected virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}
