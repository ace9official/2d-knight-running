using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UILevelUpdate : SaiBehaviour
{
   [SerializeField] protected TextMeshProUGUI levelName;

   protected override void Start()
   {
      this.levelName.text = "Lv: " + MySceneManger.instance.GetSceneId().ToString();
   }

   protected override void LoadComponents()
   {
      this.levelName = this.GetComponent<TextMeshProUGUI>();
   }
}
