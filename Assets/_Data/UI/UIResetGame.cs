using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIResetGame : MonoBehaviour
{
    public virtual void ResetGame()
    {
        MySceneManger.instance.ResetGame();
    }
}
