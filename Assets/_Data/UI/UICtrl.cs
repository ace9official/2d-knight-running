using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICtrl : SaiBehaviour
{
    public static UICtrl instance;

    [Header("UI")]
    [SerializeField] protected List<Transform> listUI;

    protected override void Awake()
    {
        base.Awake();
        if (UICtrl.instance != null) Debug.LogError("Only 1 UICtrl allow");
        UICtrl.instance = this;
    }

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadUIs();
    }

    protected virtual void LoadUIs()
    {
        foreach (Transform child in transform)
        {
            this.listUI.Add(child);
        }
        Debug.Log(gameObject.name + ": LoadUIs", gameObject);
    }

    public virtual Transform GetUI(string uiName)
    {
        foreach (Transform child in this.listUI)
        {
            if (child.name == uiName)
            {
                Transform ui = child;
                return ui;
            }
        }
        return null;
    }
}