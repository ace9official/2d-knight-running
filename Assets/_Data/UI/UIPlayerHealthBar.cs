using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class UIPlayerHealthBar : UIHealthBar
{
    [SerializeField] protected PlayerController playerController;

    protected override void LoadValue()
    {
       // this.offSet = new Vector3(0, -2.25f, 0);
    }

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadPlayerCtrl();
    }

    protected virtual void LoadPlayerCtrl()
    {
        this.playerController = this.GetComponentInParent<PlayerController>();
    }

    protected override void GetHealth()
    {
        this.currentHealth = this.playerController.damageReceiver.hp;
        if (this.currentHealth < 1) this.currentHealth = 0;
        this.maxHealth = this.playerController.damageReceiver.hpMax;
    }

    protected override void SetPos()
    {
        transform.parent.position = this.playerController.playerModel.position + this.offSet;
    }
    
    protected override void UpdateHealthText()
    {
        this.healthText.text = this.currentHealth.ToString();
    }
}
