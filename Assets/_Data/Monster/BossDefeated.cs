using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDefeated : MonoBehaviour
{
    public virtual void OnDefeated()
    {
        ScoreManager.instance.SetBossSkill();
    }
}
