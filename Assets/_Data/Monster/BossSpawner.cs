using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : Spawner
{
    public static BossSpawner bossInstance;

    [Header("Boss")]
    [SerializeField] protected int killRequire = 5;
    [SerializeField] protected bool canSpawn = false;
    [SerializeField] protected Transform ui;

    protected override void Awake()
    {
        if (BossSpawner.bossInstance != null) Debug.LogError("Only 1 BossSpawner allow");
        BossSpawner.bossInstance = this;
        this.HideAll();
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        this.canSpawn = this.CanSpawn();
        this.BossWarning();
        this.DeactiveWarning();
    }

    protected override void LoadComponents()
    {
        this.listSpawnPosName = "BossSpawnPos";
        this.spawnHolderName = "BossSpawnHolder";
        this.despawnHolderName = "BossDespawnHolder";
        this.spawnLimit = 1;
        base.LoadComponents();
    }

    public override bool CanSpawn()
    {
        if (MySceneManger.instance.IsSceneOVer()) return false;
        bool notMax = this.spawnCount < this.spawnLimit;
        bool isEnough = this.killRequire <= ScoreManager.instance.GetKill();
        return notMax && isEnough;
    }

    protected override void Spawning()
    {
        if (this.CanSpawn()) this.spawnTimer += Time.fixedDeltaTime;
        if (this.spawnTimer <= this.spawnDelay) return;
        if (!this.CanSpawn()) return;
        this.spawnTimer = 0;
//       Debug.Log("Spawned");
        Transform prefab = this.GetObj2Spawn();
        Vector3 pos = this.GetPos2Spawn();
        Transform newObj = SpawnObj(prefab, pos, this.spawnHolder);
    }

    protected virtual void BossWarning()
    {
        if (!this.canSpawn) return;
        this.ui = UICtrl.instance.GetUI("BossWarning");
        this.ui.gameObject.SetActive(true);
    }

    public virtual float BossCountDown()
    {
        float countDown = this.spawnDelay - this.spawnTimer;
        return countDown;
    }

    protected virtual void DeactiveWarning()
    {
        if (this.SpawnCount() > 0) this.ui.gameObject.SetActive(false);
    }
}
