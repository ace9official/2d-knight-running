using UnityEngine;

public class MonsterCombat : Combat
{
    [Header("Monster")]
    public MonsterCtrl monsterCtrl;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadMonsterCtrl();
    }

    protected virtual void LoadMonsterCtrl()
    {
        if (this.monsterCtrl != null) return;
        this.monsterCtrl = transform.GetComponentInParent<MonsterCtrl>();
        Debug.Log(gameObject.name + ": LoadMonsterCtrl", transform);
    }

    public override void SpawnSkill()
    {
        if (this.skillReleased) return;
        this.skillReleased = true;
        Debug.Log("SpawnSkill");
        Transform fx = FXManager.instance.Spawn("MonsterSkill");
        fx.position = this.GetStrikePoint();
        FireballFly fireballFly = fx.GetComponent<FireballFly>();
        fireballFly.Turing(PlayerController.instance.playerMovement.isTurnRight);
        fx.gameObject.SetActive(true);
    }

    protected override void Attacking()
    {

    }

    protected override Animator Animator()
    {
        return this.monsterCtrl.animator;
    }

    public override CharMoveCtrl CharMoveCtrl()
    {
        return this.monsterCtrl.monsterMovement;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {

        }
    }
}
