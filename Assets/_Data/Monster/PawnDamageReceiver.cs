using UnityEngine;

public class PawnDamageReceiver : MonsterDamageReceiver
{
    protected override void LoadValue()
    {
        base.LoadValue();
        this.hpMax = 2;
        this.killPoint = 1;
    }
}
