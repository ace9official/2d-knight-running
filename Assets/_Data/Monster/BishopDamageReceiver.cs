using UnityEngine;

public class BishopDamageReceiver : MonsterDamageReceiver
{
    protected override void LoadValue()
    {
        base.LoadValue();
        this.hpMax = 5;
        this.killPoint = 3;
    }
}
