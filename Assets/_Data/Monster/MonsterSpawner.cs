using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// ReSharper disable All

public class MonsterSpawner : Spawner
{
    public static MonsterSpawner monsterInstance;

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        this.ClearMonsterOnBossApear();
    }

    protected override void Awake()
    {
        base.Awake();
        if (MonsterSpawner.monsterInstance != null) Debug.LogError("Only 1 MonsterSpawner  allow");
        MonsterSpawner.monsterInstance = this;
        this.HideAll();
    }

    public override bool CanSpawn()
    {
        if (MySceneManger.instance.IsSceneOVer()) return false;
        bool notMax = this.spawnCount < this.spawnLimit;
        bool noBoss = BossSpawner.bossInstance.SpawnCount() == 0;
        return notMax && noBoss;
    }

    protected virtual void ClearMonsterOnBossApear()
    {
        if (!BossSpawner.bossInstance.CanSpawn()) return;
        foreach (Transform monster in this.spawnHolder)
        {
            MonsterCtrl monsterCtrl = monster.GetComponent<MonsterCtrl>();
            monsterCtrl.damageReceiver.SetDead();
        }
    }
}
