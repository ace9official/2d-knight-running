using UnityEngine;

public class MonsterCtrl : SaiBehaviour
{
    public MonsterMovement monsterMovement;
    public MonsterTarget monsterTarget;
    public CharacterController characterController;
    public DamageReceiver damageReceiver;
    public Transform model;
    public Animator animator;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadMonsterMovement();
        this.LoadDamageReceiver();
    }

    protected virtual void LoadMonsterMovement()
    {
        if (this.monsterMovement != null) return;
        this.monsterMovement = transform.Find("MonsterMovement").GetComponent<MonsterMovement>();
        this.monsterTarget = transform.Find("MonsterTarget").GetComponent<MonsterTarget>();
        this.model = transform.Find("Model");
        this.animator = this.model.GetComponentInChildren<Animator>();
        this.characterController = GetComponent<CharacterController>();
      //  Debug.Log(transform.name + "LoadMonsterMovement", gameObject);
    }

    protected virtual void LoadDamageReceiver()
    {
        if (this.damageReceiver != null) return;
        this.damageReceiver = transform.Find("DamageReceiver").GetComponent<DamageReceiver>();
//        Debug.Log(transform.name + "LoadDamageReceiver", gameObject);
    }

}