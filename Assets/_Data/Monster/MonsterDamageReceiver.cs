using System;
using UnityEngine;

public class MonsterDamageReceiver : DamageReceiver
{
    [Header("Monster")]
    [SerializeField] protected int killPoint = 1;
    [SerializeField] protected BossDefeated bossDefeated;

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadBossDeafeated();
    }

    protected virtual void LoadBossDeafeated()
    {
        this.bossDefeated = this.GetComponentInChildren<BossDefeated>();
    }

    protected override void DieCheck()
    {
        if (!this.IsDead()) return;
        ScoreManager.instance.AddSkill(this.killPoint);
        if (this.bossDefeated != null) this.bossDefeated.OnDefeated();
        this.Destroy();
    }
    
    protected override void Destroy()
    {
        //Destroy(transform.parent.gameObject);
        MonsterSpawner.monsterInstance.Despawn(transform.parent);
    }
}
