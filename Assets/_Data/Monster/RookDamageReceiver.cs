using UnityEngine;

public class RookDamageReceiver : MonsterDamageReceiver
{
    protected override void LoadValue()
    {
        base.LoadValue();
        this.hpMax = 3;
        this.killPoint = 1;
    }
}
