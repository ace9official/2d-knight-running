using UnityEngine;

public class DamageReceiver : SaiBehaviour
{
    [Header("DamageReceiver")]
    public int hp = 1;
    public int hpMax = 5;

    private void OnEnable()
    {
        this.Reborn();
    }

    public virtual int Deduct(int amount)
    {
        this.hp -= amount;
        this.DieCheck();
        return this.hp;
    }

    public virtual void SetDead()
    {
        this.Deduct(this.hp);
    }

    protected virtual void DieCheck()
    {
        if (!this.IsDead()) return;
        this.Destroy();
    }

    protected virtual void Destroy()
    {
        //Destroy(transform.parent.gameObject);
        Spawner.instance.Despawn(transform.parent);
    }

    public virtual bool IsDead()
    {
        return this.hp <= 0;
    }

    public virtual void Reborn()
    {
        this.hp = this.hpMax;
    }
}