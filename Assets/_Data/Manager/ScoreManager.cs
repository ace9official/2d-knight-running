using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ScoreManager : SaiBehaviour
{
    public static ScoreManager instance;

    [SerializeField] protected int monsterSkill = 0;
    [SerializeField] protected int bossSkill = 0;
    [SerializeField] protected int score = 0;

    protected override void Awake()
    {
        base.Awake();
        if (ScoreManager.instance != null) Debug.LogError("Only 1 ScoreManager allow");
        ScoreManager.instance = this;
    }

    public virtual int GetScore()
    {
        return this.score;
    }

    public virtual void SetScore(int _score)
    {
        this.score = _score;
    }

    public virtual void AddSkill(int add)
    {
        this.monsterSkill += add;
        this.score += add;
        SaveGameManager.instance.SaveScore();
    }

    public virtual int GetKill()
    {
        return this.monsterSkill;
    }
    
    public virtual void SetBossSkill (int add=1)
    {
        this.bossSkill += add;
    }

    public virtual int GetBossSkill()
    {
        return this.bossSkill;
    }
}
