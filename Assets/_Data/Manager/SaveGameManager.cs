using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGameManager : SaiBehaviour
{
    public static SaveGameManager instance;
    public const string SCORE = "score";

    protected override void Awake()
    {
        base.Awake();
        if (SaveGameManager.instance != null) Debug.LogError("Only 1 SaveGameManager allow");
        SaveGameManager.instance = this;
    }

    protected override void Start()
    {
        base.Start();
        this.LoadScore();
        InvokeRepeating(nameof(this.AutoSave), 1f, 5f);
    }

    protected virtual void AutoSave()
    {
        this.SaveScore();
    }
    
    public virtual void SaveScore()
    {
        SaveSystem.SetInt(SaveGameManager.SCORE, ScoreManager.instance.GetScore());
    }

    protected virtual void LoadScore()
    {
        int score = SaveSystem.GetInt(SaveGameManager.SCORE, 0);
        ScoreManager.instance.SetScore(score);
    }
}
