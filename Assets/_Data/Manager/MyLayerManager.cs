using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyLayerManager : SaiBehaviour
{
    public static MyLayerManager instance;
    [SerializeField] public int playerLayer;
    [SerializeField] public int enemyLayer;

    protected override void Awake()
    {
        base.Awake();
        if (MyLayerManager.instance != null) Debug.Log("Allow only 1 MyLayerManager");
        MyLayerManager.instance = this;
        Physics.IgnoreLayerCollision(this.playerLayer, this.enemyLayer, true);
        Physics.IgnoreLayerCollision(this.enemyLayer, this.enemyLayer, true);
    }

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.GetLayers();
    }

    private void GetLayers()
    {
        if (this.playerLayer < 1)
        {
            this.playerLayer = LayerMask.NameToLayer("Player");
            if (this.playerLayer < 0) Debug.LogError("Player layer is missing");
        }
        if (this.enemyLayer >= 1) return;
        this.enemyLayer = LayerMask.NameToLayer("Enemy");
        if (this.enemyLayer <0) Debug.LogError("Enemy layer is missing");
    }
}