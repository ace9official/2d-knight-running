using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;

public class MySceneManger : SaiBehaviour
{
    public static MySceneManger instance;
    
    [SerializeField] protected bool isSceneOver;
    [SerializeField] protected int maxLevel = 5;

    protected override void Awake()
    {
        if (MySceneManger.instance != null) Debug.LogError("Only 1 MySceneManger allow");
        MySceneManger.instance = this;
        this.maxLevel = 5;
    }

    public virtual bool IsSceneOVer()
    {
        this.isSceneOver = false;
        if (ScoreManager.instance.GetBossSkill() >= 1) this.isSceneOver = true;
        return this.isSceneOver;
    }

    public virtual void NextScene()
    {
        int nextSceneId = this.GetSceneId() + 1;
        string nextSceneName = "level_" + nextSceneId;
        SceneManager.LoadScene(nextSceneName);
    }

    public virtual void ResetScene()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }
    
    public virtual void ResetGame()
    {
        string sceneName = "level_1";
        SceneManager.LoadScene(sceneName);
    }

    public virtual int GetSceneId()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        int sceneId = int.Parse(sceneName.Replace("level_", ""));
        return sceneId;
    }
    
    public virtual bool IsMaxLevel()
    {
        return GetSceneId() >= this.maxLevel;
    }
}
