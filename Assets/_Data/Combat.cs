using System;
using UnityEngine;

public abstract class Combat : SaiBehaviour
{
    [Header("Combat")]
    public Transform strikePointLeft;
    public Transform strikePointRight;
    public float attacking = 0f;
    public float attackTimer = 0f;
    public float attackSpeed = 2f;
    public float skillSpawnDelay = 0.2f;
    public float animAttackTime = 0.5f;
    public bool canAttack = false;
    public bool skillReleased = false;

    protected abstract Animator Animator();
    public abstract void SpawnSkill();
    public abstract CharMoveCtrl CharMoveCtrl();

    protected override void Update()
    {
        this.AttackDelay();
        this.Attacking();
    }

    protected override void LoadComponents()
    {
        base.LoadComponents();
        this.LoadStrikePoint();
    }

    protected virtual void LoadStrikePoint()
    {
        if (this.strikePointLeft != null) return;
        this.strikePointLeft = transform.Find("StrikePointLeft");
        this.strikePointRight = transform.Find("StrikePointRight");
//        Debug.Log(transform.name + " :LoadStrikePoint", gameObject);
    }

    protected virtual void AttackDelay()
    {
        this.attackTimer += Time.deltaTime;
        if (this.attackTimer < this.attackSpeed) return;
        this.canAttack = true;
    }

    protected virtual void Attacking()
    {
        this.Animator().SetBool("Attacking", this.IsAttacking());
    }

    public virtual void AttackFinish()
    {
        this.canAttack = false;
        this.attackTimer = 0f;
        this.skillReleased = false;
    }

    public virtual bool IsAttacking()
    {
        return this.attacking != 0 && this.canAttack;
    }

    protected virtual Vector3 GetStrikePoint()
    {
        if (this.CharMoveCtrl().isTurnRight) return this.strikePointRight.position;
        return this.strikePointLeft.position;
    }

}